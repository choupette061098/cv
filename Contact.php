<!DOCTYPE html>

<html>
	<head>
		<title> Contactez moi!</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="style.css">
	</head>

	<body>
<?php

if(!isset($_POST['envoyer']))
{

?>
		<form id="formulaire" name="formulaire" method="post">

            <fieldset>
                <legend>Vos coordonnées</legend> <!-- Titre du fieldset --> 
         
                <label for="denom" > 
                    <span id="case"> Dénomination sociale : </span>
                    <input type="text" name="denom" id="denom" required>
                </label>
                 
                <br> <br> 
                
                <label for="activite" > 
                    <span id="case"> Activité : </span>
                    <input type="text" name="activite" id="activite" required>
                </label>
                 
                <br> <br> 
    
                <label for="adresse" >
                    <span id="case"> Adresse : </span>
                    <input type="text" id="adresse" name="adresse" required>
                </label>
                  
                <br> <br> 
    
                <label for="CP" >  
                    <span id="case"> Code postal : </span>
                    <input type="text" id="CP" name="CP" required>
                </label>
                 
                <br> <br> 
    
                <label for="ville" > 
                    <span id="case"> Ville : </span>
                    <input type="text" id="ville" name="ville" required>
                </label>
                  
                <br> <br> 

                <label for="tel" >
                    <span id="case"> Numéro de téléphone : </span>
                    <input type="tel" id="tel" name="tel" required>
                </label>
                 
                <br> <br> 

                <label for="email"> 
                    <span id="case"> E-mail : </span>
                    <input type="email" id="email" name="email" required> 
                </label>
                 
                <br> <br> 

            </fieldset>

            <fieldset>
                <legend>Votre souhait</legend>
            
                Quel type de contrat vous intéresserait ? <br />
                <p> <input type="checkbox" name="pro" id="pro" /> Contrat professionnel </label> <br /> </p>
                <p> <input type="checkbox" name="alternance" id="alternance" /> Alternance </label> <br /> </p>

                <p> Quelle est votre demande ? </label> </p> <br>
                <textarea style="width:100%;" cols="4" rows="4" name="Demande" id="Demande" required></textarea> 
             </fieldset>

                <input type="submit" value="envoyer" name="envoyer" class="envoyer">
            </form>
            
<?php
} 
else 
{
    // Si tout les champs sont remplis, sinon erreur un champs est vides
    if (isset($_POST['denom']) && isset($_POST['activite']) && isset($_POST['adresse']) && isset($_POST['ville']) && isset($_POST['CP']) && isset($_POST['tel']) && isset($_POST['email']))
    {
        $denom = $_POST['denom'];
        $activite = $_POST['activite']; 
        $adresse = $_POST['adresse']; 
        $ville = $_POST['ville'];
        $CP = $_POST['CP'];
        $tel = $_POST['tel']; 
        $email = $_POST['email']; 
        extract($_POST); //permet de créer des variables à partir du tableau $_POST

        $i = 0;
        $email_erreur = "";
        $tel_erreur = "";
        $CP_erreur = "";

        // On vérifie que chaque champs remplis sa condition
        if(!preg_match("/^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/i", $email))
        {
            $email_erreur = "Votre adresse E-Mail n'a pas un format valide";
            $i++;
        }

        //preg_match permet de faire un test basé sur une expression régulière
        if(!preg_match("/^(06|07)(\s[0-9]{2}){4}$/",$tel))
        {
            $tel_erreur = "Erreur format valide: (06 ou 07)00 00 00 00";
            $i++;
        }
        //preg_match permet de faire un test basé sur une expression régulière

        if(!ctype_digit($CP)) // 59000
        {
            $CP_erreur = "le code postal est invalide";
            $i++;
        }
        //ctype_digit détermine si une chaîne est un entier

        // Si un des champs ne remplis pas sa condition alors $i sera différent de 0 et on affichera l'erreur sinon connexion à la BDD et envoi de données
        if($i != 0)
        {
            echo'<h1>Inscription interrompue</h1>';
            echo'<p>Une ou plusieurs erreurs se sont produites pendant l incription</p>';
            if($email_erreur != "") {
                echo'<p>'.$email_erreur.'</p>';
            }
            if($tel_erreur != "") {
                echo'<p>'.$tel_erreur.'</p>';
            }
            if($CP_erreur != "") {
                echo'<p>'.$CP_erreur.'</p>';
            }

        } else {

            try 
            {
                $hostname='localhost';
                $username='root';
                $password='';

                // Connexion à la BDD
                $con = new PDO("mysql:host=localhost;dbname=projet;charset=utf8", 'root', ''); 

                $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                // Envoi des données à la BDD
                $stmt = $con->prepare("INSERT INTO contact (Denom,  Activite, Adresse, Ville, Cp, Tel, Email) VALUES (:denom, :activite, :adresse, :ville, :CP, :tel, :email)");

                $stmt -> bindParam(':denom', $_POST["denom"]);
                $stmt -> bindParam(':activite', $_POST["activite"]);
                $stmt -> bindParam(':adresse', $_POST["adresse"]);
                $stmt -> bindParam(':ville', $_POST["ville"]);
                $stmt -> bindParam(':CP', $_POST["CP"]);
                $stmt -> bindParam(':tel', $_POST["tel"]);
                $stmt -> bindParam(':email', $_POST["email"]);

                $stmt->execute();
            
                // Si la requete fonction, sinon erreur
                if ($stmt->execute()) 
                { 
                    // it worked
                    echo '<script language="javascript">';
                    echo 'alert("Votre demande à était envoyé")';
                    echo '</script>';
                } 
                else 
                {
                    // it didn't worked
                    echo $stmt->error;
                }

            } catch(PDOException $e) {
                echo ("Erreur :" . $e->getMessage());
            }

            $dbh = null;
        }
    }
    else
    {
        echo'<h1>Inscription interrompue</h1>';
        echo'<p>Veuillez remplir tout les champs</p>';
    }
}
?>
	</body>
</html>