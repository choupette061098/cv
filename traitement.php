<!DOCTYPE html>

<html>
    <head>
            <meta charset="utf-8" />
            <link rel="stylesheet" href="style.css">
   		    <title>CV</title>
    </head>
<body>
    <?php
 
 if(isset($_POST["submit"]))
 {
 $hostname='localhost';
 $username='root';
 $password='';
   
 try 
 {
 $con = new PDO("mysql:host=localhost;dbname=projet;charset=utf8", 'root', ''); 
   
 $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
 $stmt = $con->prepare("INSERT INTO contact (denom,  activite, adresse, ville, CP, tel, email) VALUES (:email, :activite, :adresse, :ville, :CP, :tel, :email)");
  
 $stmt -> bindParam(':denom', $_POST["denom"]);
 $stmt -> bindParam(':activite', $_POST["activite"]);
 $stmt -> bindParam(':adresse', $_POST["adresse"]);
 $stmt -> bindParam(':ville', $_POST["ville"]);
 $stmt -> bindParam(':CP', $_POST["CP"]);
 $stmt -> bindParam(':tel', $_POST["tel"]);
 $stmt -> bindParam(':email', $_POST["email"]);
  
 $stmt->execute();
  
if ($con->query($stmt)) 
   {
   echo ("Connexion réussie");
   }
}
    
catch(PDOException $e)
   {
   echo ("Erreur : . $e->getMessage");
   }
   
 $dbh = null;
 }

 ?>
 
    </body>
</html>